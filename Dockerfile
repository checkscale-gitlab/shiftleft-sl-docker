FROM debian:bullseye

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

RUN apt-get update && \
    apt-get upgrade --yes && \
    apt-get install --no-install-recommends --yes curl jq openjdk-11-jre && \
    mkdir --parents /opt/shiftleft/ && \
    curl -Lo /opt/shiftleft/manifest.json https://www.shiftleft.io/download/slmanifest-linux-x64.json && \
    curl -Lo /tmp/shiftleft.tar.gz "$(jq -r '.downloadURL' /opt/shiftleft/manifest.json)" && \
    echo "$(jq -r '.sha256' /opt/shiftleft/manifest.json)  /tmp/shiftleft.tar.gz" | sha256sum -c - && \
    tar -xzf /tmp/shiftleft.tar.gz -C /usr/bin/ && \
    chmod +x /usr/bin/sl && \
    ln --symbolic ./sl /usr/bin/shiftleft-cli && \
    apt-get remove --purge --yes curl jq && \
    apt-get autoremove --purge --yes && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm /tmp/shiftleft.tar.gz && \
    sl --version

SHELL ["/bin/bash", "-c"]

CMD ["sl", "--help"]
